﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScAudioManager : MonoBehaviour
{
    #region Singleton
    private static ScAudioManager _instance;
    public static ScAudioManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScAudioManager>();
                if (_instance == null)
                {
                    Debug.Log("Fatal Error : AudioManager not Found!");
                }
            }
            return _instance;
        }
    }
    #endregion

    public AudioClip scoreNormal;
    public AudioClip scoreCombo;

    public AudioClip wrongMove;
    public AudioClip tap;
    private AudioSource audioPlayer;

    void Start()
    {
        audioPlayer = GetComponent<AudioSource>();
    }

    public void PlayScore(bool isCombo)
    {
        if (isCombo)
        {
            audioPlayer.PlayOneShot(scoreCombo);
        }
        else
        {
            audioPlayer.PlayOneShot(scoreNormal);
        }
    }

    public void PlayWrong(){
        audioPlayer.PlayOneShot(wrongMove);
    }

    public void PlayTap(){
        audioPlayer.PlayOneShot(tap);
    }
}


