using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScBoardManager : MonoBehaviour
{
    #region Singleton
    public static ScBoardManager _instance = null;
    public static ScBoardManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScBoardManager>();
            }
            if (_instance == null)
            {
                Debug.LogError("Fatal Error = BoardManager not FOund");
            }
            return _instance;

        }
    }
    #endregion

    [Header("Board")]
    public Vector2Int _boardSize;
    public Vector2 _offsetTile;
    public Vector2 _offsetBoard;

    public List<Sprite> tileTypes = new List<Sprite>();
    public GameObject tilePrefab;
    public Transform panel;

    private Vector2 _startPosition;
    private Vector2 _endPosition;

    private ScTileController[,] _tiles;

    private int combo;


    //Start
    void Start()
    {
        Vector2 TileSpriteSize = tilePrefab.GetComponent<SpriteRenderer>().size;
        CreateBoard(TileSpriteSize);
    }

    void CreateBoard(Vector2 SpriteSize)
    {
        _tiles = new ScTileController[_boardSize.x, _boardSize.y];

        Vector2 totalSize = (SpriteSize + _offsetTile) * (_boardSize - Vector2.one);

        _startPosition = (Vector2)panel.position - (totalSize / 2) + _offsetBoard;
        _endPosition = _startPosition + totalSize;

        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                ScTileController newTile = Instantiate(tilePrefab, new Vector2(_startPosition.x + ((SpriteSize.x + _offsetTile.x) * x), _startPosition.y + ((SpriteSize.y + _offsetTile.y) * y)), tilePrefab.transform.rotation, panel).GetComponent<ScTileController>();
                _tiles[x, y] = newTile;

                List<int> possibleId = GetPossibleIdList(x, y);
                int newId = possibleId[Random.Range(0, possibleId.Count)];

                newTile.ChangeId(newId, x, y);
            }
        }
    }

    private List<int> GetPossibleIdList(int x, int y)
    {
        List<int> list = new List<int>();
        for (int i = 0; i < tileTypes.Count; i++)
        {
            list.Add(i);
        }

        if (x >= 2 && _tiles[x - 1, y].id == _tiles[x - 2, y].id)
        {
            list.Remove(_tiles[x - 1, y].id);
        }

        if (y >= 2 && _tiles[x, y - 1].id == _tiles[x, y - 2].id)
        {
            list.Remove(_tiles[x, y - 1].id);
        }

        return list;
    }

    //Playing General
    public bool IsAnimating //yang berarti isSwapping terikat erat dengan kapan is animating terjadi..
    {
        get
        {
            return IsSwapping || IsProcessing;
        }
    }

    public bool IsProcessing { get; set; }

    public bool IsSwapping { get; set; }

    // Playing Swapping
    public IEnumerator SwapTilePosition(ScTileController a, ScTileController b, System.Action onCompleted)
    {
        IsSwapping = true;

        Vector2Int indexA = GetTileIndex(a); //cari indexnya, aku bingung kenapa gak ditag sejak awal aja index nya.. di tile controler
        Vector2Int indexB = GetTileIndex(b);

        _tiles[indexA.x, indexA.y] = b; //swap tile di lewat board langsung;
        _tiles[indexB.x, indexB.y] = a;

        bool isRoutineACompleted = false;
        bool isRoutineBCompleted = false;

        StartCoroutine(a.MoveTilePosition(GetIndexPosition(indexB), () => { isRoutineACompleted = true; }));
        StartCoroutine(b.MoveTilePosition(GetIndexPosition(indexA), () => { isRoutineBCompleted = true; }));
        //hmmmm kayak ini kirim method ke parameter action ...
        //terus methodnya dijalankan setelah selesai...
        //sehingga bisa di cek apakah keduanya selesai alias is routine completed...

        yield return new WaitUntil(() => { return isRoutineACompleted && isRoutineBCompleted; });
        //lambda sekedar mempercepat
        Debug.Log(GetTileIndex(b) + " " + GetTileIndex(a));
        onCompleted?.Invoke();

        IsSwapping = false;
    }

    public Vector2Int GetTileIndex(ScTileController tile)
    {
        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                if (tile == _tiles[x, y]) return new Vector2Int(x, y);
            }
        }
        return new Vector2Int(-1, -1);

    }

    public Vector2 GetIndexPosition(Vector2Int index)
    {
        Vector2 tileSize = tilePrefab.GetComponent<SpriteRenderer>().size;
        return new Vector2(_startPosition.x + ((tileSize.x + _offsetTile.x) * index.x), _startPosition.y + ((tileSize.y + _offsetTile.y) * index.y));
        //oh no kenapa gak cek posisi langsung aja ya, mungkin local position..?
    }

    //Playing Match Finder
    public List<ScTileController> GetAllMatchesInBoard()
    {
        List<ScTileController> allMatchingTiles = new List<ScTileController>();

        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                List<ScTileController> tileMatched = _tiles[x, y].GetAllMatchingTiles(); //rela nulis nama variable panjang ketimbang pusing sendiri
                if (tileMatched == null || tileMatched.Count == 0)
                {
                    continue;
                }

                foreach (ScTileController tile in tileMatched)
                {
                    if (!allMatchingTiles.Contains(tile))
                    {
                        allMatchingTiles.Add(tile);//ada kemungkinan sudah ada di allMatchingTiles.. mengingat ini pengecekannya semua tile in board
                    } // bisa lebih efisien, tapi akan lebih panjang pastinya
                }

            }
        }

        return allMatchingTiles;
    }

    //Playing Tiles Processing
    public void Process()
    { // menandai kondisi saat ini
        IsProcessing = true;
        combo = 0;
        ProcessMatches();
    }

    private void ProcessMatches()
    { // memastikan MatchedTiles ada 
        List<ScTileController> matchingTiles = GetAllMatchesInBoard();

        if (matchingTiles == null || matchingTiles.Count == 0)
        {
            IsProcessing = false;
            return;
        }
        combo++;
        ScScoreManager.Instance.IncrementCurrentScore(matchingTiles.Count,combo);

        StartCoroutine(ClearMatches(matchingTiles, ProcessDrop));
    }

    private IEnumerator ClearMatches(List<ScTileController> MatchingTiles, System.Action OnCompleted)
    {
        //Destroy every Tiles
        List<bool> isAllCompleted = new List<bool>();
        for (int i = 0; i < MatchingTiles.Count; i++)
        {
            isAllCompleted.Add(false);
        }

        for (int i = 0; i < MatchingTiles.Count; i++)
        {
            int index = i;
            StartCoroutine(MatchingTiles[i].SetDestroyed(() => { isAllCompleted[index] = true; }));
        }
        // yield return new WaitUntil(()=>IsAllTrue(isAllCompleted));
        yield return new WaitUntil(() => { return IsAllTrue(isAllCompleted); });
        OnCompleted?.Invoke();
        //IsProcessing = false; //sementara
    }

    public bool IsAllTrue(List<bool> list)
    {
        foreach (bool status in list)
        {
            if (status == false)//yang penting nampak
                return false;
        }
        return true;
    }


    //Playing Tiles Dropping
    private void ProcessDrop() //process utama
    {
        Dictionary<ScTileController, int> droppingTiles = GetAllDrop(); //list tiles yang harus jatuh, plus jarak yang harus di tempuh untuk turun
        foreach (KeyValuePair<ScTileController, int> pair in droppingTiles)
        {
            Debug.Log(pair);
        }
        StartCoroutine(DropTiles(droppingTiles, ProcessDestroyAndFill));
    }

    private Dictionary<ScTileController, int> GetAllDrop() //tiles yang jatuh, !!bukan tiles yang destroyed..
    {
        Dictionary<ScTileController, int> drop = new Dictionary<ScTileController, int>();

        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                if (_tiles[x, y].IsDestroyed)
                {
                    //cek atas dari destroyed tile
                    for (int i = y + 1; i < _boardSize.y; i++)
                    {
                        if (_tiles[x, i].IsDestroyed) // kalau hancur juga, lanjut sampe ketemu atau habis
                        {
                            continue;
                        }

                        if (drop.ContainsKey(_tiles[x, i]))// kalau atasnya tidak hancur + udah masuk dic
                        {                   //tambah satu, menunjukan bahwa ada >1 yang hancur di bawah  _tiles[x,i]
                            drop[_tiles[x, i]]++;
                        }
                        else                        //belum masuk +1 jarak antara _tiles[x,i] yang harus di replace oleh beliau
                            drop.Add(_tiles[x, i], 1);
                    }

                }
            }
        }
        return drop;
    }

    private IEnumerator DropTiles(Dictionary<ScTileController, int> drop, System.Action OnCompleted)
    {// SwapTilePosition posisition

        foreach (KeyValuePair<ScTileController, int> pair in drop)
        {

            Vector2Int tileIndex = GetTileIndex(pair.Key);

            ScTileController temp = pair.Key; //atas
            _tiles[tileIndex.x, tileIndex.y] = _tiles[tileIndex.x, tileIndex.y - pair.Value]; // bawah go keatas
            _tiles[tileIndex.x, tileIndex.y - pair.Value] = temp; //  bawah = atas // atas go ke bawah

        }
        //kenapa pake couroutine kalau yield return null
        yield return null;
        OnCompleted?.Invoke();
    }

    private void ProcessDestroyAndFill()
    { //Mencari destroyed tiles -> menghidupkannya kembali
        List<ScTileController> allDestroyedTiles = GetAllDestroyedTiles();
        StartCoroutine(DestroyAndFillTiles(allDestroyedTiles, ProcessReposition));
    }

    private List<ScTileController> GetAllDestroyedTiles()
    {
        List<ScTileController> deadTiles = new List<ScTileController>();
        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                if (_tiles[x, y].IsDestroyed)
                {
                    deadTiles.Add(_tiles[x, y]);
                }
            }
        }

        return deadTiles;
    }

    private IEnumerator DestroyAndFillTiles(List<ScTileController> allDeadTiles, System.Action onCompleted)
    {
        List<int> highestIndex = new List<int>();
        for (int i = 0; i < _boardSize.x; i++)  //posisi index spawn pertama
        {
            highestIndex.Add(_boardSize.y - 1);
        }

        float SpawnHeight = _endPosition.y + tilePrefab.GetComponent<SpriteRenderer>().size.y + _offsetTile.y;
        float SpawnHeightPlus = tilePrefab.GetComponent<SpriteRenderer>().size.y + _offsetTile.y;
       
        //posisi real spawn pertama..
        //kalau aku yang harus buat kode ini, akan kubuat repot2 di atas, sehingga selanjutnya gak perlu mikir 2 3 kali hal yang sama

        foreach (ScTileController deadtile in allDeadTiles)
        {
            Vector2Int tileIndex = GetTileIndex(deadtile);
            Vector2Int targetIndex = new Vector2Int(tileIndex.x, highestIndex[tileIndex.x]);
            highestIndex[tileIndex.x]--; // kurangi index.. hingga andai ada 2 di satu index x tidak menimpa

            deadtile.transform.position = new Vector2(deadtile.transform.position.x, SpawnHeight + (SpawnHeightPlus *  (_boardSize.y - highestIndex[tileIndex.x] - 2)));
            deadtile.GenerateRandomTile(targetIndex.x, targetIndex.y);

        }// ini hanya posisi index,, posisi realnya belum ditentukan...



        yield return null;

        onCompleted?.Invoke();
    }

    private void ProcessReposition()
    {   //proses memposisikan tiles secara real/visual ->pengecekan match ulang
        StartCoroutine(RepositionTiles(ProcessMatches));
    }

    private IEnumerator RepositionTiles(System.Action onCompleted)
    {
        List<bool> isCompleted = new List<bool>();

        int i = 0;

        for (int x = 0; x < _boardSize.x; x++)
        {
            for (int y = 0; y < _boardSize.y; y++)
            {
                Vector2 targetPosition = GetIndexPosition(new Vector2Int(x, y));//getIndexposition itu cek real position berdasarkan indexnya

                if ((Vector2)_tiles[x, y].transform.position == targetPosition) //kalau udah sama skip
                {
                    continue;
                }

                isCompleted.Add(false);

                int index = i; //buat apa ini ya?

                StartCoroutine(_tiles[x, y].MoveTilePosition(targetPosition, () => { isCompleted[index] = true; }));

                i++;
            }
        }
        yield return new WaitUntil(() => { return IsAllTrue(isCompleted); }); //masih bingung kenapa ak method biasa aja not function

        onCompleted?.Invoke();
    }
}
