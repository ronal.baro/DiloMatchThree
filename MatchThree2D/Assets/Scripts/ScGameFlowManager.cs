﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScGameFlowManager : MonoBehaviour
{
    #region Singleton
    private static ScGameFlowManager _instance = null;

    public static ScGameFlowManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScGameFlowManager>();
                if (_instance == null)
                {
                    Debug.Log("Fatal Error : GameFlowManager not Found");
                }
            }
            return _instance;
        }
    }
    #endregion

    private bool _isGameOver = false;

    public bool IsGameOver
    {
        get { return _isGameOver; }
    }

    [Header ("UI")]
    public ScUIGameOver GameOverUI;
    void Start()
    {
        _isGameOver = false;
    }

    public void GameOver()
    {
        _isGameOver = true;
        ScScoreManager.Instance.SetHighscore();
        ScScoreManager.Instance.ResetCurrentScore();
        GameOverUI.Show();
    }

}
