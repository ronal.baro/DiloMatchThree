﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScScoreManager : MonoBehaviour
{
    #region Singleton

    private static ScScoreManager _instance = null;

    public static ScScoreManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScScoreManager>();

                if (_instance == null)
                {
                    Debug.LogError("Fatal Error : Score Manager not Found");
                }
            }
            return _instance;
        }
    }

    #endregion

    public int tileRatio;
    public int comboRatio;

    public int HighScore { get { return highScore; } }
    public int CurrentScore { get { return currentScore; } }

    private int currentScore;
    private int highScore;

    private void Awake() {
        if (_instance!=null){
            Destroy(this.gameObject);
        }
        else
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        ResetCurrentScore();
    }

    public void ResetCurrentScore()
    {
        currentScore = 0;
    }

    public void IncrementCurrentScore(int tileCount, int comboCount)
    {
        currentScore += (tileCount * tileRatio) * (comboCount * comboRatio);

        ScAudioManager.Instance.PlayScore(comboCount>1);
        Debug.Log("Score : " + currentScore);
    }

    public void SetHighscore()
    {
        if (currentScore > highScore)
        {
            highScore = currentScore;
        }
    }


}
