using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTileController : MonoBehaviour
{
    private static readonly Color _selectedColor = new Color(0.5f, 0.5f, 0.5f);
    private static readonly Color _normalColor = Color.white;

    private static ScTileController _previousSelected = null;
    private static readonly Vector2[] adjacentDirection = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };

    private bool isSelected = false;

    public int id;
    private ScBoardManager _board;
    private ScGameFlowManager _game;
    private SpriteRenderer _renderer;

    private static readonly float moveDuration = 0.3f;
    private static readonly float destroyGoBigDuration = 0.1f;
    private static readonly float destroyGoSmallDuration = 0.4f;

    private static readonly Vector2 sizeBig = Vector2.one * 1.2f;
    private static readonly Vector2 sizeNormal = Vector2.one;
    private static readonly Vector2 sizeSmall = Vector2.zero;


    // public bool IsProcessing { get; set; }
    // public bool IsSwapping { get; private set; }
    public bool IsDestroyed { get; private set; }


    void Awake()
    {
        _board = ScBoardManager.Instance;
        _game = ScGameFlowManager.Instance;
        _renderer = GetComponent<SpriteRenderer>();

    }
    private void Start()
    {
        // IsProcessing = false;
        // IsSwapping = false;
        IsDestroyed = false;
    }

    public void ChangeId(int id, int x, int y)
    {
        _renderer.sprite = _board.tileTypes[id];
        this.id = id;

        name = "Tile_" + id + "(" + x + "," + y + ")";
    }

    //tile resurrect
    public void GenerateRandomTile(int x, int y){
        IsDestroyed = false;
        transform.localScale = sizeNormal;

        ChangeId(Random.Range(0,_board.tileTypes.Count),x,y);
    }

    private void OnMouseDown()
    {
        if (_renderer.sprite == null || _board.IsAnimating || _game.IsGameOver)
        {
            return;
        }
        ScAudioManager.Instance.PlayTap();
        if (isSelected)
        {
            Deselect();
        }
        else
        {
            if (_previousSelected == null)
            {
                Select();
            }
            else
            {
                if (GetAllAdjacentTiles().Contains(_previousSelected))
                {

                    ScTileController otherTile = _previousSelected;
                    _previousSelected.Deselect();

                    SwapTile(otherTile, () =>
                    {
                        if (_board.GetAllMatchesInBoard().Count > 0)
                        {
                            Debug.Log("MATCH FOUND");
                            _board.Process();
                        }   //cek apakah ada yg match di board keseluruhan
                        else
                        {
                            ScAudioManager.Instance.PlayWrong();
                            SwapTile(otherTile); //else kosong -> swap balik setelah swap pertama/// 
                        }
                    });

                }
                else
                {
                    _previousSelected.Deselect();
                    Select();
                }
            }

        }

    }

    private ScTileController GetAdjacent(Vector2 castDir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir, _renderer.size.x);
        if (hit)
        {
            return hit.collider.GetComponent<ScTileController>();
        }
        return null;
    }

    public List<ScTileController> GetAllAdjacentTiles()
    {
        List<ScTileController> adjacentTiles = new List<ScTileController>();
        for (int i = 0; i < adjacentDirection.Length; i++)
        {
            adjacentTiles.Add(GetAdjacent(adjacentDirection[i]));
        }

        return adjacentTiles;
    }

    public void SwapTile(ScTileController otherTile, System.Action onCompleted = null)//aku baru tau kalau = null di parameter berarti boleh null
    {
        StartCoroutine(_board.SwapTilePosition(this, otherTile, onCompleted));
    }

    public void Select()
    {
        isSelected = true;
        _renderer.color = _selectedColor;
        _previousSelected = this;
    }

    public void Deselect()
    {
        isSelected = false;
        _renderer.color = _normalColor;
        _previousSelected = null;
    }

    public IEnumerator MoveTilePosition(Vector2 targetPos, System.Action onCompleted)
    {
        Vector2 startPos = transform.position;
        float time = 0.0f;
        yield return new WaitForEndOfFrame();
        while (time < moveDuration)
        {
            //tidak tau kenapa pakai time/duration, belum paham cara kerja lerp...
            // aku rasa float f menandakan waktu yg tersisa,, sehingga dia menyesuaikan kecepatannya...
            // jadi valuenya berubah2 kan jadinya,, aku rasa ini kurang efisien...
            //tapi terlihat lebih simple sih, jadi bukan masalah;
            transform.position = Vector2.Lerp(startPos, targetPos, time / moveDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();//bergerak setiap frame..
        }
        transform.position = targetPos;//biar akurat mungkin;
        onCompleted?.Invoke(); // kenapa pake invoke kalau gak calling method lain
    }

    //-------------------//
    //----matchFinder----//
    //-------------------//

    public List<ScTileController> GetAllMatchingTiles()
    { // 1. Cek Match di sepanjang baris hor && ver dari tile itu sendiri
        if (IsDestroyed)
        {
            return null;
        }

        List<ScTileController> allDirMatchingTiles = new List<ScTileController>();

        List<ScTileController> verticalMatchingTiles = GetOneLineMatch(new Vector2[2] { Vector2.up, Vector2.down });

        List<ScTileController> horizontalMatchingTiles = GetOneLineMatch(new Vector2[2] { Vector2.left, Vector2.right });

        if (verticalMatchingTiles != null)
        {
            allDirMatchingTiles.AddRange(verticalMatchingTiles);
        }

        if (horizontalMatchingTiles != null)
        {
            allDirMatchingTiles.AddRange(horizontalMatchingTiles);
        }

        if (allDirMatchingTiles != null && allDirMatchingTiles.Count >= 2)
        {
            allDirMatchingTiles.Add(this); // diriSendiri
        }

        return allDirMatchingTiles;

    }

    private List<ScTileController> GetOneLineMatch(Vector2[] paths) //2. cek satu baris kiri kanan || atas bawah
    {
        List<ScTileController> oneLineMatchingTiles = new List<ScTileController>();//rela tulis nama variable panjang2 ketimbang pusing sendiri

        for (int i = 0; i < paths.Length; i++)
        {
            oneLineMatchingTiles.AddRange(GetMatch(paths[i]));
        }

        if (oneLineMatchingTiles.Count >= 2)
        {
            return oneLineMatchingTiles;
        }

        return null;
    }

    private List<ScTileController> GetMatch(Vector2 castDir)
    { // 3. lanjutan atas, cek berdasarkan arah raycast 
        List<ScTileController> oneDirMatchingTiles = new List<ScTileController>();
        RaycastHit2D hit = Physics2D.Raycast(transform.position, castDir);

        while (hit)
        { //pencocokan tile akan berlanjut selama kena

            ScTileController otherTile = hit.collider.GetComponent<ScTileController>();
            if (otherTile.id != id || otherTile.IsDestroyed)// hancur atau tidaksama;
            {
                break;
            }

            oneDirMatchingTiles.Add(otherTile);

            //ganti tile pengecekan
            hit = Physics2D.Raycast(otherTile.transform.position, castDir);

        }

        return oneDirMatchingTiles;

    }

    //---------------------//
    //----TileDestroyer----//
    //---------------------//

    public IEnumerator SetDestroyed(System.Action OnCompleted)
    {
        IsDestroyed = true;
        id = -1;
        name = "TILE_NULL";

        Vector2 startSize = transform.localScale;
        float time = .0f;
        while (time < destroyGoBigDuration)
        {
            transform.localScale = Vector2.Lerp(startSize, sizeBig, time / destroyGoBigDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = sizeBig;

        startSize = transform.localScale;
        time = .0f;

        while (time < destroyGoSmallDuration)
        {
            transform.localScale = Vector2.Lerp(startSize, sizeSmall, time / destroyGoSmallDuration);
            time += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        transform.localScale = sizeSmall;
        _renderer.sprite = null;

        OnCompleted?.Invoke();
    }



}
