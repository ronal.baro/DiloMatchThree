﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTimeManager : MonoBehaviour
{
    #region Singleton
    private static ScTimeManager _instance = null;
    public static ScTimeManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScTimeManager>();
                if (_instance == null)
                {
                    Debug.Log("Fatal Error : TimeManager not Found!");
                }

            }
            return _instance;
        }
    }
    #endregion

    public int duration;
    private float time;

    void Start()
    {
        time = 0;

    }

    void Update()
    {
        if (ScGameFlowManager.Instance.IsGameOver)
        {
            return;
        }

        if (time >= duration)
        {
            ScGameFlowManager.Instance.GameOver();
            return;
        }

        time += Time.deltaTime;
    }

    public float GetRemainingTime()
    {
        return duration - time;
    }
}
