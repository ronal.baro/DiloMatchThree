﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScUIScore : MonoBehaviour
{
    public Text currentScoreText;
    public Text HighScoreText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        currentScoreText.text = ScScoreManager.Instance.CurrentScore.ToString("000000000");
        HighScoreText.text = ScScoreManager.Instance.HighScore.ToString("000000000");
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
