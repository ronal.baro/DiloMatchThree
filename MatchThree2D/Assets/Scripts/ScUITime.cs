﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScUITime : MonoBehaviour
{
    public Text TimerText;
    private int minute;
    private int second;
    void Update()
    {
        TimerText.text = GetTimeString(Mathf.FloorToInt(ScTimeManager.Instance.GetRemainingTime()));
    }

    private string GetTimeString(int timeRemaining)
    {
        minute = timeRemaining / 60;
        second = timeRemaining % 60+1;
        return string.Format("{0}:{1}", minute.ToString("00"),second.ToString("00"));
    }
}
